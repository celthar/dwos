#include "system.h"

uint8* memcpy(uint8* dest, const uint8* src, uint32 count) {
    uint32 index = 0;

    while(index < count) {
        dest[index] = src[index];
        ++index;
    }

    return dest;
}

uint8* memset(uint8* dest, uint8 val, uint32 count) {
    uint32 index = 0;

    while(index < count) {
        dest[index] = val;
        ++index;
    }

    return dest;
}

uint16* memsetw(uint16* dest, uint16 val, uint32 count) {
    uint32 index = 0;

    while(index < count) {
        dest[index] = val;
        ++index;
    }

    return dest;
}

uint8 inb(uint16 _port) {
    uint8 rv;

    __asm__ __volatile__ ("inb %1, %0" : "=a" (rv) : "dN" (_port));

    return rv;
}

uint16 inw(uint16 _port) {
    uint16 rv;

    __asm__ __volatile__ ("inw %1, %0" : "=a" (rv) : "dN" (_port));

    return rv;
}

void outb(uint16 _port, uint8 _data) {
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (_port), "a" (_data));
}
