#include "terminal.h"

static const uint32 VGA_WIDTH  = 80;
static const uint32 VGA_HEIGHT = 24;

uint32    terminal_cursor_x;
uint32    terminal_cursor_y;
uint8     terminal_color;
uint16*   terminal_buffer;

uint8 vga_makecolor(enum vga_color fg, enum vga_color bg) {
    return fg | (bg << 4);
}

uint16 vga_makeentry(char c, uint8 color) {
    uint16 c16 = c;
    uint16 color16 = color;

    return c16 | (color16 << 8);
}

void vga_putentryat(char c, uint8 color, uint32 x, uint32 y) {
    const uint32 index = y * VGA_WIDTH + x;

    terminal_buffer[index] = vga_makeentry(c, color);
}

void vga_scroll() {
    if(terminal_cursor_y < VGA_HEIGHT) {
        return;
    }

    --terminal_cursor_y;

    for(uint32 y = 1; y < VGA_HEIGHT; ++y) {
        for(uint32 x = 0; x < VGA_WIDTH; ++x) {
            const uint32 index_old = y * VGA_WIDTH + x;
            const uint32 index_new = (y - 1) * VGA_WIDTH + x;

            terminal_buffer[index_new] = terminal_buffer[index_old];
        }
    }

    for(uint32 x = 0; x < VGA_WIDTH; ++x) {
        const uint32 index = (VGA_HEIGHT - 1) * VGA_WIDTH + x;

        terminal_buffer[index] = vga_makeentry(' ', terminal_color);
    }
}

void vga_movecursor() {
    uint32 index = terminal_cursor_y * VGA_WIDTH + terminal_cursor_x;

    outb(0x3D4, 14);
    outb(0x3D5, index >> 8);
    outb(0x3D4, 15);
    outb(0x3D5, index);
}

void terminal_clearscreen() {
    for(uint32 y = 0; y < VGA_HEIGHT; ++y) {
        for(uint32 x = 0; x < VGA_WIDTH; ++x) {
            const uint32 index = y * VGA_WIDTH + x;

            terminal_buffer[index] = vga_makeentry(' ', terminal_color);
        }
    }
}

void terminal_initialize() {
    terminal_cursor_x = 0;
    terminal_cursor_y = 0;
    terminal_color = vga_makecolor(COLOR_LIGHT_GREY, COLOR_BLACK);
    terminal_buffer = (uint16*) 0xB8000;

    terminal_clearscreen();
    vga_movecursor();
}

void terminal_setcolor(enum vga_color fg, enum vga_color bg) {
    terminal_color = vga_makecolor(fg, bg);
}

void terminal_putc(char c) {
    boolean printable = TRUE;

    if(c == 0x08) {
        if(terminal_cursor_x != 0) {
            --terminal_cursor_x;
        }

        printable = FALSE;
    }

    if(c == '\r') {
        terminal_cursor_x = 0;
        
        printable = FALSE;
    }

    if(c == '\n') {
        terminal_cursor_x = 0;
        ++terminal_cursor_y;
        
        printable = FALSE;
    }

    if(printable) {
        vga_putentryat(c, terminal_color, terminal_cursor_x, terminal_cursor_y);
        
        if(++terminal_cursor_x >= VGA_WIDTH) {
            terminal_cursor_x = 0;
            ++terminal_cursor_y;
        }
    }

    vga_movecursor();
    vga_scroll();
}

void terminal_puts(const char* data) {
    uint32 datalen = strlen(data);

    for(uint32 i = 0; i < datalen; ++i) {
        terminal_putc(data[i]);
    }
}

void terminal_putb(boolean value) {
    if(value) {
        terminal_puts("TRUE");
    } else {
        terminal_puts("FALSE");
    }
}

void terminal_puti(uint32 value) {
    if(value == 0) {
        return;
    } else {
        terminal_puti(value / 10);
        terminal_putc('0' + (value % 10));
    }
}
