#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include "system.h"
#include "string.h"

enum vga_color {
    COLOR_BLACK         =  0,
    COLOR_BLUE          =  1,
    COLOR_GREEN         =  2,
    COLOR_CYAN          =  3,
    COLOR_RED           =  4,
    COLOR_MAGENTA       =  5,
    COLOR_BROWN         =  6,
    COLOR_LIGHT_GREY    =  7,
    COLOR_DARK_GREY     =  8,
    COLOR_LIGHT_BLUE    =  9,
    COLOR_LIGHT_GREEN   = 10,
    COLOR_LIGHT_CYAN    = 11,
    COLOR_LIGHT_RED     = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN   = 14,
    COLOR_WHITE         = 15,
};

void terminal_initialize();
void terminal_setcolor(enum vga_color fg, enum vga_color bg);
void temrinal_clearscreen();
void terminal_putc(char c);
void terminal_puts(const char* data);
void terminal_putb(boolean value);
void terminal_puti(uint32 value);

#endif /* __TERMINAL_H__ */
