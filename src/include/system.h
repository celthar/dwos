#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define FALSE 0
#define TRUE  1

typedef unsigned char  boolean;
typedef unsigned char  uint8;
typedef          char  sint8;
typedef unsigned short uint16;
typedef          short sint16;
typedef unsigned int   uint32;
typedef          int   sint32;

uint8*  memcpy(uint8* dest, const uint8* src, uint32 count);
uint8*  memset(uint8* dest, uint8 val, uint32 count);
uint16* memsetw(uint16* dest, uint16 val, uint32 count);
uint8   inb(uint16 _port);
uint16  inw(uint16 _port);
void    outb(uint16 _port, uint8 _data);

#endif /* __SYSTEM_H__ */
