#ifndef __STRING_H__
#define __STRING_H__

#include "system.h"

uint32  strlen(const char* str);
boolean strcmp(const char* str1, const char* str2);

#endif /* __STRING_H__ */
