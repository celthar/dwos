# Multiboot constants
.set ALIGN,    1 << 0
.set MEMINFO,  1 << 1
.set FLAGS,    ALIGN | MEMINFO
.set MAGIC,    0x1BADB002
.set CHECKSUM, -(MAGIC + FLAGS)

# Declare a section for the multiboot header
.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

# Creating a small temporary stack
.section .bootstrap_stack, "aw", @nobits
stack_bottom:
.skip 16384
stack_top:

# _start will be the entry point to the kernel (linker script)
.section .text
.global _start
.type _start, @function
_start:
    movl $stack_top, %esp
    
    call kernel_main
    
    cli
    hlt
.Lhang:
    jmp .Lhang

# Useful for debugging or call tracing
.size _start, . - _start
