#include "string.h"

uint32 strlen(const char* str) {
    uint32 result = 0;

    while(str[result] != 0) {
        ++result;
    }

    return result;
}

boolean strcmp(const char* str1, const char* str2) {
    uint32 index = 0;

    if(strlen(str1) != strlen(str2)) {
        return FALSE;
    }

    while((index < strlen(str1)) && (index < strlen(str2))) {
        if(str1[index] != str2[index]) {
            return FALSE;
        }

        ++index;
    }

    return TRUE;
}
