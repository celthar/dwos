AS = i686-elf-as
CC = i686-elf-gcc
LD = i686-elf-gcc

bootstrap:
	$(AS) src/boot.s -o obj/boot.o

kernel:
	$(CC) -Isrc/include -c src/system.c -o obj/system.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
	$(CC) -Isrc/include -c src/string.c -o obj/string.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
	$(CC) -Isrc/include -c src/driver/terminal.c -o obj/terminal.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
	$(CC) -Isrc/include -c src/kernel.c -o obj/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra

link:
	$(LD) -T linker.ld -o bin/dwos.bin -ffreestanding -O2 -nostdlib obj/boot.o obj/system.o obj/string.o obj/terminal.o obj/kernel.o -lgcc

run: 
	qemu-system-i386 -kernel bin/dwos.bin

all: bootstrap kernel link
